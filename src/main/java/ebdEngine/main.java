package ebdEngine;

import engine.*;

public class main {

    public DiagramEngineController dec;

    public static void main(String[] args) {
        try {
            System.out.println("START CUSTOM MODEL ENGINE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DiagramEngineController getCustomEngine() throws DiagramEngineController.DiagramEngineControllerException {
        this.dec = new CustomModelEngine();
        return this.dec;
    }

    public String sayHello() {
        return "Hello!";
    }
}