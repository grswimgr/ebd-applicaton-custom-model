package ebdEngine;

import engine.*;
import model.*;
import utils.UTIL;
import utils.UTIL_DataTransfer;

import java.util.*;

public class CustomModelEngine extends DiagramEngineController {

    public CustomModelEngine() throws DiagramEngineControllerException {
        super();
        System.out.println("Creating external Custom Model...");

    }

    @Override
    public void prepareRequiredParametersMap() {
        this.requiredParametersMap = new HashMap<>();

        HashMap<String, Parameter> requiredMetalParameters = new HashMap();
        requiredMetalParameters.put("Amplitude", new Parameter(
                "A",
                "A",
                "Amplitude",
                null
        ));
        HashMap<String, Parameter> requiredSemiconductorParameters = new HashMap<>();
        requiredSemiconductorParameters.put("Amplitude", new Parameter(
                "A",
                "A",
                "Amplitude",
                null
        ));
        HashMap<String, Parameter> requireDielctricParameters = new HashMap<>();
        requireDielctricParameters.put("Amplitude", new Parameter(
                "A",
                "A",
                "Amplitude",
                null
        ));

        this.requiredParametersMap.put(UTIL.MaterialType.METAL, requiredMetalParameters);
        this.requiredParametersMap.put(UTIL.MaterialType.SEMICONDUCTOR, requiredSemiconductorParameters);
        this.requiredParametersMap.put(UTIL.MaterialType.DIELECTRIC, requireDielctricParameters);
    }

    @Override
    public List<CalculationResult> getEnergyCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        Integer steps = simulationSettings.getStepsNumber();
        Double from = simulationSettings.getDataFrom();
        Double to = simulationSettings.getDataTo();

        List<CalculationResult> calculationResultsList = new ArrayList<>();
        System.out.println("Calculating Energy Result... Steps: " + steps);
        System.out.println("from: " + from + " to: " + to);
        Double thickness = 0.0;
        for(Layer layer : structure.getLayers()) {
            System.out.println("Calculating Energy for Layer: " + layer.getName());
            if(!layer.isBetween(from, to, thickness)) {
                thickness += layer.getThickness();
                System.out.println("Layer out of range. Skipping to the next layer...");
                continue;
            }

            Double layerFrom = 0.0, layerTo = 0.0;
            switch (layer.getCommonPartType(from, to, thickness)) {
                case LEFT:
                    layerFrom = thickness;
                    layerTo = to;
                    System.out.println("Layer:CommonPartType LEFT");
                    break;
                case RIGHT:
                    layerFrom = from;
                    layerTo = thickness + layer.getThickness();
                    System.out.println("Layer:CommonPartType RIGHT");
                    break;
                case INSIDE:
                    layerFrom = from;
                    layerTo = to;
                    System.out.println("Layer:CommonPartType INSIDE");
                    break;
                case OUTSIDE:
                    layerFrom = thickness;
                    layerTo = thickness + layer.getThickness();
                    System.out.println("Layer:CommonPartType OUTSIDE");
                    break;
                case ERROR:
                    // Display alert!
                    System.out.println("ERROR: Cannot Calculate - ERROR in Layer:CommonPartType");
                    break;
            }

            calculationResultsList.addAll(getEnergyCalculationResult(layerFrom, layerTo, thickness, layer, steps));

            thickness += layer.getThickness();
        }

        return calculationResultsList;
    }

    @Override
    public List<CalculationResult> getPotentialCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        Integer steps = simulationSettings.getStepsNumber();
        Double from = simulationSettings.getDataFrom();
        Double to = simulationSettings.getDataTo();

        HashMap<Double, Double> result = new HashMap<>();
        Double singleStep = (to - from) / steps;
        Integer step = 0;

        for(Double xValue = from; xValue <= to; xValue += singleStep) {
            step++;
            result.put(xValue, Math.sqrt(xValue) / 2);
        }

        CalculationResult resultTop = new CalculationResult(result, "LAYER TOP", from, to, steps, "TOP");
        CalculationResult resultMiddle = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER MIDDLE", from, to, steps, "MIDDLE");
        CalculationResult resultBottom = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER BOTTOM", from, to, steps, "BOTTOM");

        for(Double key : result.keySet()) {
            resultTop.getResultMap().put(key, resultTop.getResultMap().get(key) + 1);
            resultBottom.getResultMap().put(key, resultBottom.getResultMap().get(key) - 1);
        }

        List<CalculationResult> resultsList = new ArrayList<>();
        resultsList.add(resultTop);
        resultsList.add(resultMiddle);
        resultsList.add(resultBottom);

        return resultsList;
    }

    @Override
    public List<CalculationResult> getElectricFieldCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        Integer steps = simulationSettings.getStepsNumber();
        Double from = simulationSettings.getDataFrom();
        Double to = simulationSettings.getDataTo();

        HashMap<Double, Double> result = new HashMap<>();
        Double singleStep = (to - from) / steps;
        Integer step = 0;

        for(Double xValue = from; xValue <= to; xValue += singleStep) {
            step++;
            result.put(xValue, Math.cos(xValue));
        }

        CalculationResult resultTop = new CalculationResult(result, "LAYER TOP", from, to, steps, "TOP");
        CalculationResult resultMiddle = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER MIDDLE", from, to, steps, "MIDDLE");
        CalculationResult resultBottom = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER BOTTOM", from, to, steps, "BOTTOM");

        for(Double key : result.keySet()) {
            resultTop.getResultMap().put(key, resultTop.getResultMap().get(key) + 1);
            resultBottom.getResultMap().put(key, resultBottom.getResultMap().get(key) - 1);
        }

        List<CalculationResult> resultsList = new ArrayList<>();
        resultsList.add(resultTop);
        resultsList.add(resultMiddle);
        resultsList.add(resultBottom);

        return resultsList;
    }

    @Override
    public List<CalculationResult> getChargeDensityCalculationResultList(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        Integer steps = simulationSettings.getStepsNumber();
        Double from = simulationSettings.getDataFrom();
        Double to = simulationSettings.getDataTo();

        HashMap<Double, Double> result = new HashMap<>();
        Double singleStep = (to - from) / steps;
        Integer step = 0;

        for(Double xValue = from; xValue <= to; xValue += singleStep) {
            step++;
            result.put(xValue, Math.sqrt(xValue));
        }

        CalculationResult resultTop = new CalculationResult(result, "LAYER TOP", from, to, steps, "TOP");
        CalculationResult resultMiddle = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER MIDDLE", from, to, steps, "MIDDLE");
        CalculationResult resultBottom = new CalculationResult((HashMap<Double, Double>)result.clone(), "LAYER BOTTOM", from, to, steps, "BOTTOM");

        for(Double key : result.keySet()) {
            resultTop.getResultMap().put(key, resultTop.getResultMap().get(key) + 1);
            resultBottom.getResultMap().put(key, resultBottom.getResultMap().get(key) - 1);
        }

        List<CalculationResult> resultsList = new ArrayList<>();
        resultsList.add(resultTop);
        resultsList.add(resultMiddle);
        resultsList.add(resultBottom);

        return resultsList;
    }


    public List<CalculationResult> getEnergyCalculationResult(Double from, Double to, Double startingPoint, Layer layer, Integer steps) {
        HashMap<Double, Double> result = new HashMap<>();
        Double singleStep = (to - from) / steps;
        Integer step = 0;
        Double amplitudeValue = 1.0;
        switch (layer.getMaterial().getType()) {
            case METAL:
                amplitudeValue = layer.getMaterial().getParameters().get("Amplitude").getValue();
                for(Double xValue = from; xValue <= to; xValue += singleStep) {
                    step++;
                    result.put(xValue, Math.sin(xValue) * amplitudeValue);
                }
                break;
            case DIELECTRIC:
                amplitudeValue = layer.getMaterial().getParameters().get("Amplitude").getValue();
                for(Double xValue = from; xValue <= to; xValue += singleStep) {
                    step++;
                    result.put(xValue, Math.sin(xValue) * amplitudeValue);
                }
                break;
            case SEMICONDUCTOR:
                amplitudeValue = layer.getMaterial().getParameters().get("Amplitude").getValue();
                for(Double xValue = from; xValue <= to; xValue += singleStep) {
                    step++;
                    result.put(xValue, Math.sin(xValue) * amplitudeValue);
                }
                break;
        }

        CalculationResult resultTop = new CalculationResult(result, layer.getName(), from, to, steps, "TOP");
        CalculationResult resultMiddle = new CalculationResult((HashMap<Double, Double>)result.clone(), layer.getName(), from, to, steps, "MIDDLE");
        CalculationResult resultBottom = new CalculationResult((HashMap<Double, Double>)result.clone(), layer.getName(), from, to, steps, "BOTTOM");

        for(Double key : result.keySet()) {
            resultTop.getResultMap().put(key, resultTop.getResultMap().get(key) + 1);
            resultBottom.getResultMap().put(key, resultBottom.getResultMap().get(key) - 1);
        }

        List<CalculationResult> resultsList = new ArrayList<>();
        resultsList.add(resultTop);
        resultsList.add(resultMiddle);
        resultsList.add(resultBottom);

        return resultsList;
    }
}
